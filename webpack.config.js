const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

module.exports = {
    module: {
        rules: [
            {
                test: /\.scss$/,
                use: ["style-loader", "css-loader", "sass-loader"]
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                use: ["babel-loader"]
            }
        ]
    },
    entry: {
        index: path.resolve(__dirname, "source", "index.jsx")
    },
    plugins: [
        new HtmlWebpackPlugin({
            favicon: path.resolve(__dirname, "public", "favicon/page_icon.jpg"),
            template: path.resolve(__dirname, "source", "index.html")
        })
    ],
    output: {
        path: path.resolve(__dirname, "build")
    }
};