import "./style.scss";
//import {returnHTML, wrapData, FetchCode} from 'html-highlight';
//import {FetchHandler} from "fetch-simple"
//import {CookieHandler} from "cookie-middleware"

import React, { useEffect, useState } from "react";
import { render } from "react-dom"

const Container = () => {
    return (
        <div className="container__outer">
            <div className="container__inner-up">
                <h1> Content visible before scrolling</h1>
                <h1> Content visible before scrolling</h1>
                <h1> Content visible before scrolling</h1>
                <h1> Content visible before scrolling</h1>
                <h1> Content visible before scrolling</h1>
                <h1> Content visible before scrolling</h1>
                <h1> Content visible before scrolling</h1>
            </div>
            <div className="container__inner">
                <h1> Page Headline </h1>
                <p> Some Paragraph about everything and nothing </p>
                <p> Another Paragraph describing the depths of needing placeholder content.</p>
                <h1> Page Headline </h1>
                <p> Some Paragraph about everything and nothing </p>
                <p> Another Paragraph describing the depths of needing placeholder content.</p>
                <h1> Page Headline </h1>
                <p> Some Paragraph about everything and nothing </p>
                <p> Another Paragraph describing the depths of needing placeholder content.</p>
                <h1> Page Headline </h1>
                <p> Some Paragraph about everything and nothing </p>
                <p> Another Paragraph describing the depths of needing placeholder content.</p>
            </div>
        </div>
    )
}

const Page = (props) => {
    return (
        <div className="page">
            {props.children}
        </div>
    )
}

const Render = () => (
    <Page>
        <Container />
    </Page>
)




render(Render(), document.getElementById("root"))